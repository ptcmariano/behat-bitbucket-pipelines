# Behat Bitbucket Pipelines

This is a example simple testing in Friendly Pix firebase example app

> Project to use behat test in bitbucket pipeline

## Context

- https://www.atlassian.com/continuous-delivery/how-to-get-to-continuous-integration

- Similar tutorial: https://www.atlassian.com/continuous-delivery/continuous-integration-tutorial

## This tests

Here used a default config with: behat --init

A simple test with goutte in features/bootstrap/FeatureContext.php

PS: Using MinkContext some defaults behaviors

## To run local

> php vendor/bin/behat
